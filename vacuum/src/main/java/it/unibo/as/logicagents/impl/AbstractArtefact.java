package it.unibo.as.logicagents.impl;

import alice.tuprolog.Prolog;
import alice.tuprolog.Terms;
import alice.tuprolog.exceptions.InvalidObjectIdException;
import alice.tuprolog.lib.OOLibrary;
import it.unibo.as.logicagents.Artefact;
import it.unibo.as.logicagents.Environment;

import java.util.Objects;

/**
 * Base class for all artifacts
 */
public abstract class AbstractArtefact<E extends Environment> implements Artefact<E> {
    private final E environment;

    protected AbstractArtefact(E environment) {
        this.environment = Objects.requireNonNull(environment);
    }

    @Override
    public E getEnvironment() {
        return environment;
    }

    @Override
    public void attachTo(Prolog agent, String name) throws InvalidObjectIdException {
        final OOLibrary library = (OOLibrary) agent.getLibrary(OOLibrary.class.getName());
        library.register(Terms.atom(Objects.requireNonNull(name)), this);
    }

    protected void log(String format, Object... args) {
        System.out.printf(format, args);
    }
}
