package it.unibo.as.logicagents.impl;

import it.unibo.as.logicagents.Sucker;
import it.unibo.as.logicagents.VacuumEnvironment;
import it.unibo.as.logicagents.Vector2D;

/**
 * This is an actuator artifact letting the agent clean up the environment position it is lying upon
 */
public class SuckerImpl extends AbstractArtefact<VacuumEnvironment> implements Sucker {

    public SuckerImpl(VacuumEnvironment environment) {
        super(environment);
    }

    @Override
    public boolean cleanUp() throws Exception {
        final Vector2D position = getEnvironment().getVacuumPosition();
        getEnvironment().cleanUp(position);

        return !getEnvironment().isDirty(position);
    }
}
