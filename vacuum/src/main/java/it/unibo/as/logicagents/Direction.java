package it.unibo.as.logicagents;

/**
 * A relative direction
 */
public enum Direction {
    FORWARD,
    LEFT,
    RIGHT,
    BACKWARD;
}
